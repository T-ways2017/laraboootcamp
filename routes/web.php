<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/Apropos', 'AproposController@index')->name('A-propos');

Route::resource('chiefs', 'ChiefController');

Route::get('/dones', 'ChiefController@dones')->name('dones');


Route::get('/undones', 'ChiefController@undones')->name('undones');

Route::put('change/{id}', 'ChiefController@change')->name('changed');

Route::put('/chiefs/{chief}/affectedTo/{user}', 'ChiefController@affectedTo')->name('chiefs.affectedTo');

