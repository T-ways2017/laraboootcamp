<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\Chief;
use App\User;
use Illuminate\Support\Facades\Auth;
use Mockery\Undefined;

class ChiefController extends Controller
{
     /***
     * define users for all users
     * get all users of users model
     */
    public $users;

    public function __construct()
    {
         $this->users = User::getAllUsers();

    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

                if (!Auth::user() ) {

                    notify()->warning("<span class='badge badge-dark'>Are you admin or user !?</span>");
                    return ('<p> Login before or Are you authentified ? </p>');
                } else {

                     //get user connected id
                     $userId = Auth::user()->id;
                    //display all chiefs created by a curente user
                    $chiefs = Chief::where(['creator_id'=>$userId])->orderBy('id','desc')->paginate(3);
                    //$chiefs = Chief::all();
                    //$chiefs = Chief::paginate(3);
                    //get users form Users
                        $users = $this->users;
                    //return ('hello new Chief');
                    //dd($chiefs);
                notify()->info("<span class='badge badge-dark'>welcome to chiefs all list!</span>");
                return view('chiefs.index', compact('chiefs', 'users'));
                }




    }
    /**
     * Display alls chiefs where active egal 1 then dones.
     *
     * @return \Illuminate\Http\Response
     */
    public function dones()
    {
        //get user connected id
        $userId = Auth::user()->id;
        //display all chiefs created by a curente user and active egal done
       $chiefs = Chief::where(['creator_id'=>$userId,'active'=>'1'])->orderBy('id','desc')->paginate(3);
        // $chiefs = Chief::where('active',1)->paginate(3);
           //get users form Users
          $users = $this->users;
          notify()->info("<span class='badge badge-dark'>Done items!</span>");
        return view('chiefs.index',compact('chiefs','users'));

    }

    /**
     * Display alls chiefs where active egal 0 then undones.
     *
     * @return \Illuminate\Http\Response
     */
    public function undones()
    {

          //get user connected id
        $userId = Auth::user()->id;
        //display all chiefs created by a curente user and active egal undone
       $chiefs = Chief::where(['creator_id'=>$userId,'active'=>'0'])->orderBy('id','desc')->paginate(3);
        //$chiefs = Chief::where('active',0)->paginate(3);
          //get users form Users
          $users = $this->users;

          notify()->info("<span class='badge badge-dark'>UnDone items!</span>");
        return view('chiefs.index',compact('chiefs','users'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        //get user connected id
      $userId = Auth::user()->id;

        return view('chiefs.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //return ('hello new Chief store');

        $chief = new Chief();

        $chief->name = $request->name;
        $chief->surename = $request->surename;
        $chief->birth = $request->birthday;
        $chief->descript = $request->description;
        $chief->photo = $request->photo;
        $chief->active = $request->active;
        $chief->creator_id = $request->creator;
        //dd($chief);
       $chief->save();

       notify()->success("<span class='badge badge-dark'>Chiefs added successfully !</span>");
       return redirect()->route('chiefs.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  Chief $chief
     * @return \Illuminate\Http\Response
     */
    public function edit(Chief $chief)
    {
        //
        return view('chiefs.edit', compact('chief'));
    }

    /**
     * changes active values of each chiefs .
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function change(Request $request, $id)
    {
        //change value of active

      // $chief->active = $request->active;
       //$chief->update();
       //return back();

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  Chief $chief
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Chief $chief)
    {
        //  dd($chief);
        //update
        $chief->update($request->all());
        return redirect()->route('chiefs.index');

    }

    /**
     * Remove the specified resource from storage.
     *
     *@param Chief $chief
     * @return \Illuminate\Http\Response
     */
    public function destroy(Chief $chief)
    {
        //delete one chiefs
        $chief->delete();
        return back();
    }


    /**
     * .
     *
     * @param Chief $chief
     * @param User $user
     * @return \Illuminate\Http\Response
     */
    public function affectedTo(Chief $chief, User $user)
    {
        //
        $chief->affectedTo_id = $user->id;

        $chief->affectedBy_id = Auth::user()->id;

        $chief->update();

        return back();
    }



}
