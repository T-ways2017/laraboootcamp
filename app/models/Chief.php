<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;

class Chief extends Model
{
    //
    protected $fillable = [
        'name', 'surename', 'birth', 'descript', 'photo' , 'active'
    ];
}
