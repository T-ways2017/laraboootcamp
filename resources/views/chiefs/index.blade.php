@extends('../../layouts/app')
@section('content')
<div class="row">

   <div class="container">
        <p class="container">
        ALL CHIEFS <i class="fas fa-plus"></i>
        </p>
        <p class="text-center">
            <a href="{{ route('chiefs.create') }}" class="btn btn-lg btn-primary"> <i class="fas fa-plus"></i> Create</a>
            @if (Route::currentRouteName() == 'chiefs.index')
                <a href="{{ route('dones') }}" class="btn btn-lg btn-light">DONES </a>
                <a href="{{ route('undones') }}" class="btn btn-lg btn-success">UNDONES</a>

            @elseif(Route::currentRouteName() == 'dones')
               <a href="{{ route('chiefs.index') }}" class="btn btn-lg btn-primary">ALL </a>
               <a href="{{ route('undones') }}" class="btn btn-lg btn-success">UNDONES</a>

            @elseif(Route::currentRouteName() == 'undones' )
             <a href="{{ route('dones') }}" class="btn btn-lg btn-light">DONES </a>
             <a href="{{ route('chiefs.index') }}" class="btn btn-lg btn-primary">All</a>
            @endif


        </p>
        <p>
            @foreach ($chiefs as $item)
            {{-- DISPLAYING INFORMAIONS ABOUT EACH CHIEFS  --}}
                <div class="alert alert-{{ $item->active == 1 ? 'primary':'secondary' }}">
                    <div class="alert-heading">
                        {{$item->name}} | {{$item->surename}}
                        <span class="badge badge-info">create :{{ $item->created_at }} || update :{{ $item->updated_at }}</span>
                    </div>
                    <div class="row">

                        <p class="col">
                            {{$item->birth}}
                        </p>
                        <p class="col">
                            {{$item->descript}}
                        </p>
                        <p class="col">
                            {{$item->photo}}
                        </p>
                        <p class="col">
                     {{-- DISPLAYING INFORMAIONS ABOUT EACH CHIEFS  --}}
                            {{-- button affect user  --}}
                              <form action="#" method="POST">
                                  @method('PUT')
                                    <div class="dropdown mx-2">
                                        <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        affected :
                                        </button>
                                        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                            {{-- fetch all users in dropdown elements --}}
                                            @foreach ($users as $user)
                                            <a class="dropdown-item" href="#">{{ $user->name}}</a>
                                            @endforeach

                                        </div>
                                    </div>
                             </form>


                            {{-- button active on unactive --}}

                            <form action="{{ route('chiefs.update',$item->id) }}" method="POST">
                                @method('PUT')
                                @csrf

                                 {{-- name --}}
                                 <input type="text" name="name" id="name" value="{{ $item->name }}" required hidden>
                                 {{-- surename --}}
                                 <input type="text" name="surename" id="name" value="{{ $item->surename }}" required hidden>
                                 {{-- descripi --}}
                                 <input type="text" name="description" id="description" value="{{ $item->descript }}" required hidden>
                                 {{-- photo --}}
                                 <input type="text" name="photo" id="photo" value="{{ $item->photo }}" required hidden>
                                 {{-- birthday --}}
                                 <input type="date" name="birthday" id="birthday" value="{{ $item->birth }}" required hidden>

                                <input type="number" value="{{ $item->active == 1 ? '0':'1'  }}" name="active" required hidden>
                                <button class="btn btn-sm btn-{{ $item->active == 1 ? 'success':'warning' }}">
                                    @if ($item->active == 1)
                                    {{ "done" }}
                                    @else
                                    {{ "undone" }}
                                    @endif
                                </button>
                            </form>
                         {{-- button active on unactive --}}

                        </p>

                        {{-- button eDIT --}}
                        <p class="col">
                            {{-- button edit --}}
                            <a href="{{ route('chiefs.edit',$item->id) }}" class="btn btn-sm btn-warning mx-1">EDIT</a>

                            {{-- button delete --}}
                           <form action="{{ route('chiefs.destroy',$item->id) }}" method="POST">
                               @method('DELETE')
                                @csrf
                            <button class="btn btn-sm btn-danger">DELETE</button>
                           </form>

                        </p>
                    </div>

                </div>
            @endforeach
            {{$chiefs->links()}}
        </p>

   </div>

</div>


@endsection
