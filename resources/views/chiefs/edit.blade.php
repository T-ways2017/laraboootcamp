@extends('../../layouts/app')
@section('content')
<div class="row">

   <div class="container">
        <p class="container">
        Editing Chiefs
        </p>
        <div class="alert alert-primary">
            <div class="alert-heading">
                Editing chief id <span class="badge badge-danger">{{ $chief->id }}</span>
            </div>
            <div>
                <a href="{{ route('chiefs.index') }}" class=" btn btn-dark ">Go to List</a>
            </div>

                <form action="{{ route('chiefs.update',$chief->id) }}" method="POST">
                    @method('PUT')
                    @csrf
                    <div class="text-center">














                        <div class="form-group">
                            <label for="name">Name</label>
                             {{-- name --}}
                             <input type="text" name="name" id="name" value="{{ $chief->name }}" required>
                        </div>

                        <div class="form-group">
                            <label for="surename">Surename</label>
                           {{-- surename --}}
                           <input type="text" name="surename" id="name" value="{{ $chief->surename }}" required>
                        </div>

                        <div class="form-group">
                            <label for="description">Description</label>
                             {{-- descripi --}}
                             <input type="text" name="descript" id="description" value="{{ $chief->descript }}" required>
                        </div>

                        <div class="form-group">
                            <label for="photo">photo</label>
                            {{-- photo --}}
                            <input type="text" name="photo" id="photo" value="{{ $chief->photo }}" required>
                        </div>

                        <div class="form-group">
                            <label for="birthday">Birthday</label>
                            {{-- birthday --}}
                            <input type="date" name="birth" id="birthday" value="{{ $chief->birth }}" required>
                        </div>

                        <div class="form-group">
                            <label for="actived">Active:<span class="badge badge-secondary">   Acctuacliy:{{$chief->active}} </span></label>

                            <select class="form-control" id="actived" name="active" required>
{{--  echo with php native commande to change avtice value  --}}
                                <option value="{{$chief->active}}">
                                    @php
                                    if ($chief->active == '1') {

                                        echo 'Active';
                                    }else {

                                        echo 'Unactive';
                                    }
                                    @endphp
                                </option>
                                {{-- change value of active --}}
                                @php
                                if ($chief->active == '1') {
                                    $chief->active ='0';
                                }else {
                                    $chief->active ='1';
                                }
                                @endphp
                                {{-- set new value --}}
                                <option value="{{ $chief->active }}">
                                @php
                                if ($chief->active == '1') {
                                    echo 'Active';
                                }else {
                                    $chief->active ='1';
                                    echo 'Unative';
                                }
                                @endphp
                                </option>

                            </select>
                        </div>

                        <div class="form-group">
                            <button type="submit" class="btn btn-primary">
                                Update
                            </button>

                        </div>
                    </div>
                </form>

        </div>

   </div>

</div>


@endsection
