@extends('../../layouts/app')
@section('content')
<div class="row">

   <div class="container">
        <p class="container">
        CREATE NEW CHIEF
        </p>
        <div class="alert alert-primary">
            <div class="alert-heading">
                Adding new Chief
            </div>
            <div>
                <a href="{{ route('chiefs.index') }}" class=" btn btn-dark ">Go to List</a>
            </div>

                <form action="{{ route('chiefs.store') }}" method="POST">
                    @csrf
                    <div class="text-center">
                        <div class="form-group">
                            <label for="name">Name</label>
                            <input type="text" name="name" id="name">
                        </div>

                        <div class="form-group">
                            <label for="surename">Surename</label>
                            <input type="text" name="surename" id="name">
                        </div>

                        <div class="form-group">
                            <label for="birthday">Birthday</label>
                            <input type="date" name="birthday" id="birthday">
                        </div>

                        <div class="form-group">
                            <label for="description">Description</label>
                            <input type="text" name="description" id="description">
                        </div>

                        <div class="form-group">
                            <label for="photo">photo</label>
                            <input type="text" name="photo" id="photo">
                        </div>

                        <div class="form-group">
                            <label for="actived">Active:</label>
                            <select class="form-control" id="actived" name="active">
                                <option value="1">Active</option>
                                <option value="0">Unactive</option>
                            </select>
                        </div>
                        <div>
                            <input type="text" name="creator" value="{{ Auth::user()->id }}"  hidden>
                        </div>

                        <div class="form-group">
                            <button type="submit" class="btn btn-primary">
                                add
                            </button>

                        </div>
                    </div>
                </form>

        </div>

   </div>

</div>


@endsection
