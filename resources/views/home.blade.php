@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Dashboard') }}</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>

                    @endif

                    {{ __('You are logged in!') }}
                    <div>
                        <h3 class="bg-aqua">  welcome  : {{ Auth::user()->name }}</h3>
                     </div>
                     <div>
                         <a href="{{ route("chiefs.index") }}">
                             go to Your  Chiefs listes <i class="fa fa-address-book" aria-hidden="false"></i>
                         </a>
                     </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
