<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Chief;
use Faker\Generator as Faker;
use Illuminate\Support\Str;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(App\Models\Chief::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'surename' => $faker->name,
        'birth' => $faker->date(),
        'descript' => $faker->sentence(7),
        'photo' => $faker->sentence(1),
        'active' => $faker->numberBetween(0,1),
    ];
});
