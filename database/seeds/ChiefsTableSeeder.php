<?php

use Illuminate\Database\Seeder;

class ChiefsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        factory(App\Models\Chief::class, 20)->create();
    }
}
